package steps;

import Pages.login;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class CommonStepDefinition {
    login loginObject;

    public CommonStepDefinition()
    {
        loginObject=new login();

    }
    @Given("^user is  on homepage$")
    public void user_is_on_homepage() throws Throwable {
        Assert.assertTrue(loginObject.launchUrl());

    }

    @When("^user navigates to Login Page$")
    public void user_navigates_to_Login_Page() throws Throwable {
        Assert.assertTrue(loginObject.loginToPage());

    }

    @When("^user enters username and Password$")
    public void user_enters_username_and_Password() throws Throwable {
        Assert.assertTrue(loginObject.logIn());

    }

    @Then("^success message is displayed$")
    public void success_message_is_displayed() throws Throwable {
        Assert.assertTrue(loginObject.verifyMessage());

    }
}
